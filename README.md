Write me a shadow
==================

Workshop at ÉSAD Valence 3-4 mai 2012   
Ludi Loiseau and Eric Schrijver


*   Web Specimen: <http://ospublish.constantvzw.org/workshop/write-me-a-shadow/>
*   Source Files: <http://osp.kitchen/workshop/write-me-a-shadow/>
*   Photo Gallery: <http://ospublish.constantvzw.org/images/write-me-a-shadow>

Even if a digital font is much more easily modified and reinterpreted
than its counterpart in lead, most digital fonts are distributed under
licences that prohibit modification and redistribution.

Typefaces released under an Open Source license, like the typefaces on
the [OSP Foundry](http://osp.kitchen), the [League of Moveable Type](https://www.theleagueofmoveabletype.com) and also the [Google Web
Fonts](https://fonts.google.com/) have licenses that enable re-appropriation.

At the occasion of the workshop ‘Write me a Shadow’ we start from a
collection of free and open source typefaces. We follow several
approaches, through scripting and drawing, to generate new versions of
these typefaces, contoured, shadowed, extruded, cut and redrawn. The
typefaces are superimposed using
[Colorfont.js](http://manufacturaindependente.com/colorfont/), using
JavaScript and @font-face to create multi-coloured typefaces for the
web. We use a [modified
version](http://ospublish.constantvzw.org/workshop/write-me-a-shadow/js/multiple-color-font.js)
of this project developed by [Manufactura
Independente](http://blog.manufacturaindependente.org/) (Ana Carvalho &
Ricardo Lafuente).
